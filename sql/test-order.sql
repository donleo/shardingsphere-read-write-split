/**
  数据分片测试sql
 */
CREATE TABLE `t_order0` (
  `order_id`  int(11) NULL ,
  `user_id`  int(11) NULL ,
  `goods_id`  int(11) NULL ,
  `goods_name`  varchar(200) NULL
)ENGINE=InnoDB DEFAULT charset= utf8mb4 row_format=dynamic;

CREATE TABLE `t_order1` (
  `order_id`  int(11) NULL ,
  `user_id`  int(11) NULL ,
  `goods_id`  int(11) NULL ,
  `goods_name`  varchar(200) NULL
)ENGINE=InnoDB DEFAULT charset= utf8mb4 row_format=dynamic;

/**
  读写分离测试sql
 */
CREATE TABLE `cuser` (
  `id`  int(11) NULL ,
  `name`  varchar(64) NULL
)ENGINE=InnoDB DEFAULT charset= utf8mb4 row_format=dynamic;