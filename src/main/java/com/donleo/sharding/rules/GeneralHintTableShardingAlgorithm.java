package com.donleo.sharding.rules;

import org.apache.shardingsphere.sharding.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.sharding.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

public class GeneralHintTableShardingAlgorithm implements HintShardingAlgorithm<String> {
    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, HintShardingValue<String> shardingValue) {
        Collection<String> list = new ArrayList<>();
        for(String s : shardingValue.getValues()){
            for (String table : availableTargetNames){
                if(table.endsWith(s)){
                    list.add(table);
                }
            }
        }
        return list;
    }

    @Override
    public Properties getProps() {
        return null;
    }

    @Override
    public void init(Properties props) {

    }

    @Override
    public String getType() {
        return "GENERAL-TABLE-HIT";
    }
}
