package com.donleo.sharding;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 * @author liangd
 * @since 2021-03-04 15:59
 */
public final class DataSourceUtil {

    private static final int PORT = 3306;

    /**
     * 通过 Hikari 数据库连接池创建 DataSource
     * @param ip                ip地址
     * @param username          用户名
     * @param password          密码
     * @param dataSourceName    数据库
     * @return                  数据源
     */
    public static DataSource createDataSource(String ip, String username, String password, String dataSourceName) {
        HikariDataSource result = new HikariDataSource();
        result.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
        result.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=UTF-8", ip, PORT, dataSourceName));
        result.setUsername(username);
        result.setPassword(password);
        return result;
    }
}
