package com.donleo.sharding;

/**
 * @author Chen ChunJiang
 * date 2022-08-18
 */
public class ToString {

    public static void main(String[] args) {
        System.out.println("select" +
                " m.advertiser_id as advertiserId," +
                        " SUM(cost) as cost1," +
                        " convert(SUM(cost),decimal(12,2)) as cost," +
                        " convert(SUM(cost) / SUM(`show`)*1000,decimal(12,2)) as avgShowCost" +
                        "  from" +
                        " jl_auth_account_advertiser m" +
                        " LEFT JOIN jl_advertiser_daily_data c ON c.advertiser_id = m.advertiser_id  and stat_day BETWEEN '2021-03-01' and '2022-12-31'" +
                        " where c.cost>0" +
                        "  group by m.advertiser_id" +
                        "  order by m.advertiser_id" +
                        "  LIMIT 100,20");
    }
}
